@extends('layouts.app')
@section('content')
<div class="container">
    <img src="http://127.0.0.1:8000/img/banner.jpg">

    @if(Session::has('success'))
        <div class="alert alert-success">{{Session::get('success')}}</div>
    @endif
    <!-- table-responsive -->
    <table class="table mt-4 table-bordered ">
        <div class="float-left">
            <a class="btn btn-success" href="{{route('home')}}"> إضافة </a>
        </div>
        <thead >
            <tr class="text-center">
            <th scope="col" colspan="2">Action</th>
            <th scope="col"> ملاحظات  </th>
            <th scope="col"> المهمة والإنجاز </th>
            <th scope="col"> اليوم </th>
            <th scope="col"> التاريخ </th>
                
        
            </tr>
        </thead>
    @if($projects)

        @foreach ($projects as $project)
            <tbody>
            
                <tr>
               

                        <!-- <td> <a href="{{url    ('projects/preview')}} "  class="btn btn-info" > Preview</a></td> -->
                    <td>
                        <form  action="{{route('projects.delete', $project->id)}}" method="POST">
                            @csrf
                            @method('DELETE')
                            <button class="btn btn-danger" type="submit">حذف</button>
                        </form>
                    </td> 
                    <td> 
                        <a href="{{route('projects.edit', $project->id)}}" class="btn btn-primary">تعديل </a></td>

                    <td dir="rtl">{{ $project->notes }}</td>
                    <td dir="rtl">  {{$project->description}}  </td>
                    <td>
            <?php 

                if($project->day == 1) {
                     $day ='............';
                    } else if ($project->day==2) { 
                        $day ='الإثنين';
                 } else if ($project->day==3) { 
                    $day ='الثلاثاء';
                } else if ($project->day==4) { 
                    $day ='الأربعاء';
                } else if ($project->day==5) { 
                    $day ='الخميس';
                } else {
                    $day = 'الجمعة';
                } 
             ?> 
             {{$day}}
             </td> 
                    <td>{{ date('Y/m/d', strtotime($project->created_at)) }}</td>
             </td> 
                </tr>
            </tbody>
        @endforeach
        {{ $projects->links() }}
        @endif

    
    </table>
    
</div>
@endsection