
<div class="container"><img src="http://127.0.0.1:8000/img/banner.jpg">  </div>
<div class="container">
                @csrf
                <div class="row">           
                    <div class="col">
                            <label class="col-sm-12 control-label " for="day"><i><b> :اليوم</b></i></label>
                            <div class="col">
                                <select name="day" id="day" class="form-control @error('day') is-invalid @enderror" value="@if(isset($project)) {{$project->day}}  @endif {{old('day')}} ">
                                        @error('day')
                                            <p class="text-danger"> {{ $message }} </p>
                                        @enderror
                                    <option value="1">.............</option>
                                    <option value="2">الإثنين </option>
                                    <option value="3">الثلاثاء</option>
                                    <option value="4"> الأربعاء</option>
                                    <option value="5">الخميس </option>
                                    <option value="6">الجمعة</option> 
                                </select>
                            </div>
                    </div>

                    <div class="col">
                        <label class="col-sm-12 control-label " for="date"><i><b> :التاريخ</b></i></label>
                            <input type="date" class="form-control @error ('date')  is-invalid @enderror" name="created_at" id="created_at" value="@if(isset($project)) {{$project->date}}  @endif  {{old('date')}}">
                            @error('date')
                                <p class="text-danger"> {{$message }}</p>
                            @enderror
                    </div>
                </div>

                    {{-- //           body_script           / --}}
                    <div class="form-group">
                        <label class="col-sm-12 control-label " for="description"><i><b>المهمة والإنجاز </b></i></label>
                        <textarea dir="rtl" class="col-sm-12 control-label " name="description" id="description" class="form-control @error('description') is-invalid @enderror" cols="30" rows="10" placeholder=" ....أكتب الوصف هنا " value="@if(isset($project)) {{$project->description}}  @endif  {{old('description')}}"></textarea>  
                        @error('description')
                            <p class="text-danger"> {{$message}} </p>
                        @enderror
                    </div>

                    <div class="row">
                        <div class="col">
                                <label class="col-sm-12 control-label " for="notes" > <i><b>ملاحظات </b></i> </label>
                                <input dir="rtl" type="text" class="form-control  @error('notes') is-invalid @enderror" name="notes" id="notes" value="@if(isset($project)) {{$project->notes}}  @endif {{old('notes')}} ">
                                @error('notes')
                                    <p class="text-danger"> {{ $message }} </p>
                                @enderror
                        </div>
                    </div>
            <br>
                
                    <div class="text-sm-center">
                        <div class="form-group mt-3">
                                <input type="submit" value="حفظ"  class="btn btn-success">
                                <a href="{{route('projects.index')}}"  class="btn btn-danger"> إلغاء </a>
                        </div>
                    </div>

</div>
