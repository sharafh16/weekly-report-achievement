@extends('layouts.app')
@section('content')
<div class="p-3"> 
      
        <div class="float-left ">
            <a href="{{ route('projects.index') }}" class="btn btn-info">Back</a>
        </div>
             <h2 class=" text-sm-center">Project Details</h2>
    </div>
  
  
    <table class="col-sm-12 text-sm-center">
            <thead >
                <tr class="text-center">
                <th scope="col" colspan="2">Action</th>
                <th scope="col"> المهمة والإنجاز </th>
                <th scope="col">ألاسم</th>
                <th scope="col"> التاريخ </th>
                <th scope="col">اليوم</th>
                    
                    
                </tr>
            </thead>
           
            <tbody>
            
                <tr>
                 
                    <td> <h4> {{$project->description}} </h4> </td>
                    <td> <h4> {{$project->name}} </h4> </td>
                    <td>{{$project->date}}</td>
                    <td>
            <?php 

                if($project->day == 1) {
                     $day ='الاثْنَيْنِ';
                 } else if ($project->day==2) { 
                    $day ='الثُّلاَثَاءِ';
                } else if ($project->day==3) { 
                    $day ='الأربعاء';
                } else if ($project->day==4) { 
                    $day ='الخميس';
                } else {
                    $day = 'الجمعة';
                } 
             ?> 
             {{$day}}
             </td>  
                   
                </tr>
             </tbody>
        </table>

        <hr>
      
    </table>
    @endsection
