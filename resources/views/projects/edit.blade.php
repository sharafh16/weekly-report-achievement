@extends('layouts.app')
@section('content')    
    <form action="{{ route('projects.update') }}" method="POST">
        @method('PATCH')
        @include('projects.form')
        <input type="hidden" name="project_id" value="@if(isset($project)) {{$project->id}}  @endif ">
    </form>
@endsection