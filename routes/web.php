<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('auth/login');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/show','ShowController@index')->name('show');

Route::get('projects/index', 'ProjectController@index')->name('projects.index'); //route
Route::get('projects/create', 'ProjectController@create')->name('projects.create');  //route
Route::post('projects/store', 'ProjectController@store')->name('projects.store'); //form only
Route::get('projects/{project}/show', 'ProjectController@show')->name('projects.show'); //wildcard
Route::get('projects/{project}/edit', 'ProjectController@edit')->name('projects.edit'); //wildcard
Route::patch('projects/update', 'ProjectController@update')->name('projects.update'); //form hidden
Route::delete('projects/{project}/delete', 'ProjectController@destroy')->name('projects.delete'); //form with wildcard


Route::get('projects/preview', 'ProjectController@preview')->name('project.previw'); //route


