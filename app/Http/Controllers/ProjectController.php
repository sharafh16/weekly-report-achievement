<?php

namespace App\Http\Controllers;

use Illuminate\Support\Arr;

use Illuminate\Support\Facades\Auth;
use Session;

use App\Project;
use App\User;
use Illuminate\Http\Request;

class ProjectController extends Controller
{
    public function index()
    {
        return view('projects.index', ['projects' => User::find(Auth::id())->projects()->paginate(5)]);
    }

    public function create()
    {
        return view('projects.create');
    }

    public function store(Request $request)
    {
        Project::create($this->validateAttributes());
        return redirect()->route('projects.index')->with('success', 'CREATED SUCCESSFULLY');
    }

    public function show(Project $project)
    {
        Session::put('project_name',$project->name);

        return view('projects.show', ['project' => $project] );
    }

    public function edit(Project $project)
    {
        return view('projects.edit', ['project' => $project] );
    }
    public function preview()
    {
        $project  = Project::find(Session::get('project_id'));
        
        
        return view('projects.preview', ['project' => $project]);
    }

    public function update()
    {
        Project::findOrFail(request('project_id'))->update($this->validateAttributes());
        return redirect()->route('projects.index')->with('success', 'UPDATED SUCCESSFULLY');
    }


    public function destroy(Project $project)
    {
        Project::find($project->id)->delete();

        return redirect()->back()->with('success', 'Deleted Successfully');
    }

    public function validateAttributes() {
         return Arr::add(request()->validate([
            'description'   => 'required',
            'notes'   => '',
            'day'   => 'required',
            'created_at'      => 'required'  
        ]), 'user_id',  Auth::id());
    }
}
